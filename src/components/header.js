import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

class Header extends Component {
  render() {
    return(
      <Navbar>
        <Nav>
          <LinkContainer exact={true} to="/">
            <NavItem eventKey={1}>Sign in</NavItem>
          </LinkContainer>
          <LinkContainer to="/signout">
            <NavItem eventKey={2}>Sign out</NavItem>
          </LinkContainer>
        </Nav>
      </Navbar>
    );
  }
}

export default Header;
