import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

import Header from './header';

class App extends Component {
  render() {
    return (
      <div>React simple starter</div>
    );
  }
}

export default class AppRoutes extends Component {
  render () {
    return (
      <Router>
        <div>
          <Header />
          <Route exact path='/' component={App} />
          <Route path='/signout' component={App} />
        </div>
      </Router>
    );
  }
}
